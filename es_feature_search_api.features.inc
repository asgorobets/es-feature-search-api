<?php
/**
 * @file
 * es_feature_search_api.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function es_feature_search_api_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_search_api_index().
 */
function es_feature_search_api_default_search_api_index() {
  $items = array();
  $items['baseball'] = entity_import('search_api_index', '{
    "name" : "Baseball",
    "machine_name" : "baseball",
    "description" : null,
    "server" : "elasticsearch",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "nid" : { "type" : "integer" },
        "vid" : { "type" : "integer" },
        "type" : { "type" : "string" },
        "title" : { "type" : "text" },
        "field_home_batters" : { "type" : "list\\u003Ctext\\u003E" },
        "field_home_game_number" : { "type" : "integer" },
        "field_home_pitcher" : { "type" : "text" },
        "field_home_score" : { "type" : "integer" },
        "field_home_team" : { "type" : "text" },
        "field_outs" : { "type" : "integer" },
        "field_visiting_batters" : { "type" : "list\\u003Ctext\\u003E" },
        "field_visiting_pitcher" : { "type" : "text" },
        "field_visiting_score" : { "type" : "integer" },
        "field_visiting_team" : { "type" : "text" },
        "search_api_language" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_home_batters" : true,
              "field_home_pitcher" : true,
              "field_home_team" : true,
              "field_visiting_batters" : true,
              "field_visiting_pitcher" : true,
              "field_visiting_team" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_home_batters" : true,
              "field_home_pitcher" : true,
              "field_home_team" : true,
              "field_visiting_batters" : true,
              "field_visiting_pitcher" : true,
              "field_visiting_team" : true
            },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_home_batters" : true,
              "field_home_pitcher" : true,
              "field_home_team" : true,
              "field_visiting_batters" : true,
              "field_visiting_pitcher" : true,
              "field_visiting_team" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_home_batters" : true,
              "field_home_pitcher" : true,
              "field_home_team" : true,
              "field_visiting_batters" : true,
              "field_visiting_pitcher" : true,
              "field_visiting_team" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function es_feature_search_api_default_search_api_server() {
  $items = array();
  $items['elasticsearch'] = entity_import('search_api_server', '{
    "name" : "Elasticsearch",
    "machine_name" : "elasticsearch",
    "description" : "",
    "class" : "search_api_elasticsearch_service",
    "options" : { "host" : "127.0.0.1", "port" : "9200", "facet_limit" : "10" },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
